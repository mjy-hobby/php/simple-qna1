<?php
if(!isset($_GET['seq']))
{
    ?><script>alert("잘못된 접근입니다."); history.back();</script><?php
    exit();
}
$seq = intval($_GET['seq']);
if(!$seq)
{
    ?><script>alert("잘못된 번호입니다."); history.back();</script><?php
    exit();
}
include ".htdbconfig.php";
if(!($result = $conn->query("SELECT title, q, a FROM qna WHERE seq = $seq")))
{
    ?><script>alert("질문 조회 실패"); history.back();</script><?php
    $conn->close();
    exit();
}
if(!($row = $result->fetch_assoc()))
{
    ?><script>alert("질문 없음"); history.back();</script><?php
    $conn->close();
    exit();
}
$result->close();
?><!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8" />
        <title><?=htmlspecialchars($row['title'])?></title>
        <link rel="stylesheet" href="style.css" />
    </head>
    <body>
        <h1><?=htmlspecialchars($row['title'])?></h1>
        <hr />
        <p><?=nl2br(htmlspecialchars($row['q']))?></p>
        <hr />
        <p><?=nl2br(htmlspecialchars($row['a']))?></p><?php
$conn->close();
?>

    <a href="index.php">질문 목록</a>
    </body>
</html>
