<?php
error_reporting(0);
if($_SERVER['REQUEST_METHOD'] !== 'POST')
{
    ?><script>alert("잘못된 접근입니다."); history.back();</script><?php
    exit();
}
include ".htdbconfig.php";
if(!isset($_POST['title'], $_POST['text']))
{
    ?><script>alert("뭔가 잘못됐는데?"); history.back();</script><?php
    $conn->close();
    exit();
}
$stmt = $conn->prepare("INSERT INTO qna (title, q) VALUES (?, ?)");
if(!$stmt)
{
    ?><script>alert("서버 오류. 관리자에게 문의 바랍니다."); history.back();</script><?php
    $conn->close();
    exit();
}
$stmt->bind_param("ss", $_POST['title'], $_POST['text']);
if(!$stmt->execute())
{
    ?><script>alert("질문을 올리지 못했습니다."); history.back();</script><?php
    $stmt->close();
    $conn->close();
    exit();
}
$stmt->close();
$conn->close();
?><!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>Simple 1:1 Q&amp;A</title>
    </head>
    <body>
        <script>
            location.href = "index.php";
        </script>
    </body>
</html>
