<?php
include ".htdbconfig.php";
$page_size = 10;
$page_index = 0;
if(isset($_GET['s']))
{
    $tmp = intval($_GET['s']);
    if($tmp) $page_size = $tmp;
}
if(isset($_GET['i']))
{
    $page_index = intval($_GET['i']) - 1;
}
$get_i = $page_index + 1;
$page_index *= $page_size;
if(!($result = $conn->query("SELECT seq, title FROM qna WHERE a IS NOT NULL ORDER BY seq DESC LIMIT $page_index, $page_size")))
{
    ?><script>alert("목록 조회 실패"); history.back();</script><?php
    $conn->close();
    exit();
}
if(!($row = $result->fetch_assoc()))
{
?><!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>Simple 1:1 Q&amp;A</title>
        <link rel="stylesheet" href="style.css" />
    </head>
    <body>
        <h1>Simple 1:1 Q&amp;A</h1>
        <hr />
        <p>답변된 질문이 없습니다. <a href="q.html">질문을 남겨보세요!</a></p>
    </body>
</html><?php
    $conn->close();
    exit();
}
?><!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>Simple 1:1 Q&amp;A</title>
        <link rel="stylesheet" href="style.css" />
    </head>
    <body>
        <h1>Simple 1:1 Q&amp;A</h1>
        <hr />
        <table>
            <thead>
                <tr>
                    <th>번호</th>
                    <th>제목</th>
                </tr>
            </thead>
            <tbody><?php
do
{
    echo "
                <tr>
                    <td>$row[seq]</td>
                    <td><a href=\"view.php?seq=$row[seq]\">" . htmlspecialchars($row['title']) . "</a></td>
                </tr>";
}
while($row = $result->fetch_assoc());
$result->free();
?>

            </tbody>
        </table>
        <p><?php
if(!($count = $conn->query("SELECT COUNT(*) FROM qna WHERE a IS NOT NULL")))
{
    echo '
            개수를 로드하지 못했습니다.';
}
else
{
    $count = $count->fetch_array()[0]; // 여기에 예외 처리가 필요한가요?
    $page_count = floor($count / $page_size);
    $page_start = floor($page_index / 10 / $page_size) * 10;
    $tmp = $page_size != 10 ? "s=$page_size&i=" : 'i=';
    if($page_start)
    {
        echo "
            <a href=\"index.php?$tmp$page_start\">이전</a>";
    }
    for($i = 0; $i < 10;)
    {
        $i++;
        $temp = $page_start + $i;
        if($temp > floor(($count - 1) / $page_size) + 1) break;
        echo $get_i == $temp ? "
            <b>$temp</b>" : "
            <a href=\"index.php?$tmp$temp\">$temp</a>";
    }
    if(($page_start + $page_size) * 10 < $count)
    {
        $temp = $page_start + 11;
        echo "
            <a href=\"index.php?$tmp$temp\">다음</a>";
    }
}
?>

        </p>
        <a href="q.html">질문하기</a>
        <a href="list.php">답변 없는 질문 목록</a>
    </body>
</html><?php
$conn->close();
?>