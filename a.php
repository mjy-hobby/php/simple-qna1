<?php
error_reporting(0);
if($_SERVER['REQUEST_METHOD'] !== 'POST')
{
    if(!isset($_GET['seq']))
    {
        ?><script>alert("잘못된 접근입니다."); history.back();</script><?php
        exit();
    }
    $seq = intval($_GET['seq']);
    if(!$seq)
    {
        ?><script>alert("잘못된 번호입니다."); history.back();</script><?php
        exit();
    }
    include ".htdbconfig.php";
    ?><!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>Simple 1:1 Q&amp;A</title>
        <link rel="stylesheet" href="style.css" />
        <script src="script.js"></script>
    </head>
    <body>
        <form action="a.php" method="post" name="qna">
            <h1>답변하기</h1>
            <div><?php
    if(!($result = $conn->query("SELECT q FROM qna WHERE seq = $seq")))
    {
        echo "
                <a href=\"view.php?seq=$seq\">질문 보기</a>";
    }
    echo nl2br(htmlspecialchars($result->fetch_array()[0]));
    $result->close();
    $conn->close();
    ?></div>
            <input type="hidden" name="seq" value="<?=$seq?>" />
            <textarea name="text" onkeydown="resize(this)" onkeyup="resize(this)"></textarea>
            <input type="submit" value="답변하기" />
        </form>
    </body>
</html><?php
    exit();
}
if(!isset($_POST['text'], $_POST['seq']))
{
    ?><script>alert("뭔가 잘못됐는데?"); history.back();</script><?php
    exit();
}
$seq = intval($_POST['seq']);
if(!$seq)
{
    ?><script>alert("잘못된 번호입니다."); history.back();</script><?php
    exit();
}
include ".htdbconfig.php";
$stmt = $conn->prepare("UPDATE qna SET a = ?, time_a = CURRENT_TIMESTAMP WHERE a IS NULL AND seq = ?");
if(!$stmt)
{
    ?><script>alert("서버 오류. 관리자에게 문의 바랍니다."); history.back();</script><?php
    $conn->close();
    exit();
}
$stmt->bind_param("si", $_POST['text'], $seq);
if(!$stmt->execute())
{
    ?><script>alert("답변을 올리지 못했습니다."); history.back();</script><?php
    $stmt->close();
    $conn->close();
    exit();
}
$stmt->close();
$conn->close();
?><!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>Simple 1:1 Q&amp;A</title>
    </head>
    <body>
        <script>
            location.href = "view.php?seq=<?=$seq?>";
        </script>
    </body>
</html>
