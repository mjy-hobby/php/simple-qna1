-- 데이터베이스와 사용자가 없는 경우에만 (적당히 바꿔서) 실행해 주세요.
-- MariaDB 기준으로 작성된 쿼리입니다. 데이터베이스에 따라 적당히 바꿔주세요.
-- CREATE DATABASE php;
-- GRANT ALL PRIVILEGES ON php.* TO 'php'@'%' IDENTIFIED BY 'phppassword';
-- USE php;

CREATE TABLE qna
(
    seq INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(255) NOT NULL,
    q VARCHAR(12000) NOT NULL,
    a VARCHAR(12000) NULL,
    time_q TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    time_a TIMESTAMP NULL,
    INDEX(time_q),
    INDEX(time_a, time_q)
);
